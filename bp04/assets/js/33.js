const op1 = document.getElementById('op1');
const op2 = document.getElementById('op2');
const op3 = document.getElementById('op3');
const op4 = document.getElementById('op4');
const op5 = document.getElementById('op5');
const op6 = document.getElementById('op6');
const op7 = document.getElementById('op7');
const op8 = document.getElementById('op8');
const op9 = document.getElementById('op9');
const op10 = document.getElementById('op10');
const op11 = document.getElementById('op11');
const op12 = document.getElementById('op12');
const op13 = document.getElementById('op13');
const op14 = document.getElementById('op14');

op1.addEventListener("click", cambiar1);
op2.addEventListener("click", cambiar2);
op3.addEventListener("click", cambiar3);
op4.addEventListener("click", cambiar4);
op5.addEventListener("click", cambiar5);
op6.addEventListener("click", cambiar6);
op7.addEventListener("click", cambiar7);
op8.addEventListener("click", cambiar8);
op9.addEventListener("click", cambiar9);
op10.addEventListener("click", cambiar10);
op11.addEventListener("click", cambiar11);
op12.addEventListener("click", cambiar12);
op13.addEventListener("click", cambiar13);
op14.addEventListener("click", cambiar14);

var mensajeNo = "";
var valorCorrectoA = 0;
var valorCorrectoB = 0;
var valorCorrectoC = 0;
var valorCorrectoD = 0;
var valorCorrectoE = 0;
var valorCorrectoF = 0;
var valorCorrectoG = 0;

function cambiar1() {


  op1.classList.add('correctoGreen');
  valorCorrectoA = 1;
  mensajeNo="";
  correcto(valorCorrectoA,mensajeNo);

}

function cambiar2() {
  // op2.classList.add('correctoRed');
  mensajeNo ="Volver a intentar";
  error(op2,mensajeNo);



}

function cambiar3() {
  mensajeNo ="Volver a intentar";
  error(op3,mensajeNo);

}

function cambiar4() {

  op4.classList.add('incorrectoGreen');
  valorCorrectoB = 1;
  mensajeNo="";
  correcto(valorCorrectoB,mensajeNo);
}

function cambiar5() {
  mensajeNo ="Volver a intentar";
  error(op5,mensajeNo);

}

function cambiar6() {


  op6.classList.add('incorrectoGreen');
  valorCorrectoC = 1;
  mensajeNo="";
  correcto(valorCorrectoC,mensajeNo);

}
function cambiar7() {
  op7.classList.add('correctoGreen');
  valorCorrectoD = 1;
  mensajeNo="";
  correcto(valorCorrectoD,mensajeNo);
}

function cambiar8() {
  mensajeNo ="Volver a intentar";
  error(op8,mensajeNo);
}



function cambiar9() {
  op9.classList.add('correctoGreen');
  valorCorrectoE = 1;
  mensajeNo="";
  correcto(valorCorrectoE,mensajeNo);
}

function cambiar10() {
  mensajeNo ="Volver a intentar";
  error(op10,mensajeNo);
}




function cambiar11() {
  mensajeNo ="Volver a intentar";
  error(op11,mensajeNo);

}

function cambiar12() {
  op12.classList.add('incorrectoGreen');
  valorCorrectoF = 1;
  mensajeNo="";
  correcto(valorCorrectoF,mensajeNo);
}



function cambiar13() {
  op13.classList.add('correctoGreen');
  valorCorrectoG = 1;
  mensajeNo="";
  correcto(valorCorrectoG,mensajeNo);
}

function cambiar14() {
  mensajeNo ="Volver a intentar";
  error(op14,mensajeNo);
}


function correcto(valor, mensajeNo) {
  if ((valorCorrectoA == 1) && (valorCorrectoB=1) && (valorCorrectoC==1) && (valorCorrectoD==1) && (valorCorrectoE==1) && (valorCorrectoF==1) && (valorCorrectoG==1)){
      mensajeCorrecto(mensajeNo, 'quitarOverly()', 'Continuar');
      botonesNavegacion();
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
  }
  else{
    mensajeCorrecto(mensajeNo, 'quitarOverly()', 'Continuar');
  }
}


function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  var correcto = document.getElementById("final_ok");
  invisible.style.display = "none";
  error.style.display = "none";
  correcto.style.display = "none";
}


function error(op, mensaje) {
  mensajeIncorrecto(mensaje, 'quitarOverly()', 'Continuar');
  var x = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  x.style.display = "block";
  error.style.display = "block";
  inicio(op);
}

function inicio(op) {
 switch(op.id){
  case "op1":
    op.className = "fill imgFondoCheck "
    break;
  case "op2":
    op.className = "fill imgFondoFalse"
    break;
  case "op3":
    op.className = "fill imgFondoCheck"
    break;
  case "op4":
    op.className = "fill imgFondoFalse"
    break;
  case "op5":
    op.className = "fill imgFondoCheck"
    break;
  case "op6":
    op.className = "fill imgFondoFalse"
    break;
  case "op7":
    op.className = "fill imgFondoCheck"
    break;
  case "op8":
    op.className = "fill imgFondoFalse"
    break;
  case "op9":
    op.className = "fill imgFondoCheck"
    break;
  case "op10":
    op.className = "fill imgFondoFalse"
    break;
  case "op11":
    op.className = "fill imgFondoCheck"
    break;
  case "op12":
    op.className = "fill imgFondoFalse"
    break;
  }
  // document.getElementById(op).className="btn btn-default foto_centro fill opcion";
}
